% Der Perceptron-Algorithmus
% Robert Pollak; Roland Richter
% Juli 2016


Die Aufgabe
-----------

![](Kekse_Overlay_6.png)\

Finde Entscheidungsgrenzen zwischen den einzelnen Klassen


Maschinelle Lernverfahren
-------------------------

Finde Entscheidungsgrenzen zwischen den einzelnen Klassen -- **wie?**

. . .

* Entscheidungsbäume
* _k_ nächste Nachbarn
* Künstliche neuronale Netze
    - Perceptron
* Regelbasierte Systeme
* Support Vector Machines


Perceptron-Algorithmus
----------------------

![](800px-Diskriminanzfunktion.png)\

Der Perceptron-Algorithmus lernt, _zwei_ Klassen zu unterscheiden, wenn sie
    _linear trennbar_ sind



Mathematische Form
------------------

Gegeben: zwei Punktmengen $N$ ("negativ") und $P$ ("positiv")

Gesucht: eine Gerade, die $N$ und $P$ trennt

Was heißt das mathematisch?

![](PerceptronDemo_Seed2454675_1.png)\


Mathematische Form
------------------

Gegeben: zwei Mengen $N$ ("negativ") und $P$ ("positiv") in $\mathbf{R}^d$

Gesucht: $w \in \mathbf{R}^d, b \in \mathbf{R}$ so, dass

$\forall n \in N: w^T n + b < 0$ und $\forall p \in P: w^T p + b > 0$

![](PerceptronDemo_Seed2454675_2.png)\


Perceptron-Algorithmus
----------------------

Idee:

* Beginne mit einer beliebigen Geraden

* Solange noch Punkte auf der falschen Seite liegen:

    - wähle einen beliebigen Punkt aus, der auf der falschen Seite liegt
    - drehe und verschiebe die Gerade so, dass der Punkt danach auf richtigen
        Seite liegt

Demo
----

![PerceptronDemo oder <http://www.openprocessing.org/sketch/135826>](PerceptronDemo_Screen.png)


Perceptron-Algorithmus
----------------------

Der Perceptron-Algorithmus

* findet eine Lösung, falls es überhaupt eine gibt

* läuft für immer weiter, falls es keine Lösung gibt

* findet _eine_ Lösung, aber nicht die "beste"

* benötigt im schlimmsten Fall sehr lange (exponentielle Laufzeit)

* ist eines der ältesten Verfahren des maschinellen Lernens 
    (Frank Rosenblatt, 1958)

