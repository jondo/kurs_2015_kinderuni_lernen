@echo off

::
:: Calls pandoc's markdown -> LaTex conversion
::

setlocal
path "%PROGRAMFILES%\MiKTeX 2.9\miktex\bin\";C:\bin\pandoc;%PATH%

REM HACK: For matching red color of the list item markers, add the line
REM   \setbeamercolor{item}{fg=darkred}
REM to MiKTeX\2.9\tex\latex\beamer\base\themes\color\beamercolorthemebeaver.sty

@echo on

%~d1
cd %~p1

del %~n1.pdf

if not exist *.template goto mdsimple

:: Which TEMPLATE file to take if there is more than one?!

set TEMPLATE_FILE=
for %%T in (*.template) do set TEMPLATE_FILE=%%T

if not exist *.csl goto mdtemplate
if not exist *.bib goto mdtemplate

:: Which CSL or BIB file to take if there is more than one?!

set CSL_FILE=
for %%S in (*.csl) do set CSL_FILE=%%S
set BIB_FILE=
for %%U in (*.bib) do set BIB_FILE=%%U

pandoc -f markdown -t beamer --template=%TEMPLATE_FILE% -V colortheme:beaver --csl=%CSL_FILE% --bibliography=%BIB_FILE% -o %~n1.pdf %1
goto showpdf

:mdtemplate
pandoc -f markdown -t beamer --template=%TEMPLATE_FILE% -V colortheme:beaver -o %~n1.pdf %1
goto showpdf

:mdsimple
pandoc -f markdown -t beamer -V colortheme:beaver -o %~n1.pdf %1

:showpdf
start %~n1.pdf
