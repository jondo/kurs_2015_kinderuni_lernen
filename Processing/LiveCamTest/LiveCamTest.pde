import gab.opencv.*;
import processing.video.*;
import java.awt.*;

Capture video;
OpenCV opencv;

final int Width  = 640; // or 1024;
final int Height = 480; // or 768;
final int Scale = 2;


void setup() {
    size(Width, Height);
    video = new Capture(this, Width/Scale, Height/Scale);
    opencv = new OpenCV(this, Width/Scale, Height/Scale);
    opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);  

    video.start();
}


void draw() {
    scale(Scale);
    
    opencv.loadImage(video);

    image(video, 0, 0);

    noFill();
    stroke(0, 255, 0);
    strokeWeight(3);

    Rectangle[] faces = opencv.detect();
    println(faces.length);

    for (int i = 0; i < faces.length; i++) {
        println(faces[i].x + "," + faces[i].y);
        rect(faces[i].x, faces[i].y, faces[i].width, faces[i].height);
    }
}


void captureEvent(Capture c) {
    c.read();
}

